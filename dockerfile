FROM archlinux:latest

LABEL name="docker-arm-toolchain"
LABEL vendor="Silesian Aerospace Technologies"
LABEL summary="Arch Linux based gcc-arm-none-eabi (11.2.1-1.2) toolchain with CMake and other utils."

ARG INSTALL_PATH=/opt

ARG TOOLCHAIN_URL=https://github.com/xpack-dev-tools/arm-none-eabi-gcc-xpack/releases/download/v11.2.1-1.2/xpack-arm-none-eabi-gcc-11.2.1-1.2-linux-x64.tar.gz
ARG TOOLCHAIN_NAME=xpack-arm-none-eabi-gcc-11.2.1-1.2

ARG SONAR_SCANNER_VERSION=sonar-scanner-4.7.0.2747-linux
ARG SONAR_SCANNER_URL=https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.7.0.2747-linux.zip

ARG SONAR_HOST_URL=https://sonarcloud.io
ARG BUILD_WRAPPER_VERSION=build-wrapper-linux-x86.zip
ARG BUILD_WRAPPER_URL=${SONAR_HOST_URL}/static/cpp/${BUILD_WRAPPER_VERSION}

ARG QEMU_URL=https://gitlab.com/api/v4/projects/33158668/jobs/artifacts/sat-master/raw/qemu-system-arm?job=amd64-qemu-system-arm-linux-container

COPY ./tools/* ${INSTALL_PATH}/tools/

RUN chmod -R 777 ${INSTALL_PATH}/tools

RUN pacman-key --init

RUN pacman -Syu --noconfirm

RUN pacman-key --refresh-keys

RUN pacman -Syu --noconfirm gcc clang make cmake python python-pip git doxygen wget unzip pixman dtc ninja gdb jq

RUN python -m pip install gcovr==5.0

# Install the toolchain
RUN wget -O ${TOOLCHAIN_NAME}.tar.gz ${TOOLCHAIN_URL} \
    && tar xf ${TOOLCHAIN_NAME}.tar.gz -C ${INSTALL_PATH} \
    && rm ${TOOLCHAIN_NAME}.tar.gz

# Install the sonar-scanner
RUN curl -sSLo sonar-scanner.zip ${SONAR_SCANNER_URL} \
    && unzip -o sonar-scanner.zip \
    && mv ${SONAR_SCANNER_VERSION} ${INSTALL_PATH}/sonar-scanner

# Install the build-wrapper
RUN curl -sSLo ${BUILD_WRAPPER_VERSION} ${BUILD_WRAPPER_URL} \
    && unzip -oj ${BUILD_WRAPPER_VERSION} -d ${INSTALL_PATH}/build-wrapper

RUN curl --create-dirs -sSLo ${INSTALL_PATH}/qemu/qemu-system-arm ${QEMU_URL} \
    && chmod +x ${INSTALL_PATH}/qemu/qemu-system-arm 

# Add toolchain and sonar binaries to PATH
ENV PATH="${INSTALL_PATH}/${TOOLCHAIN_NAME}/bin:${INSTALL_PATH}/build-wrapper:${INSTALL_PATH}/sonar-scanner/bin:${INSTALL_PATH}/qemu:${INSTALL_PATH}/tools:${PATH}"
